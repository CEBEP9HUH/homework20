// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActorBase.h"
#include "SnakePartBase.h"

// Sets default values
ASnakeActorBase::ASnakeActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SnakePartSize = 10.f;
	MovementSpeed = 1.f;
	LastDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeActorBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(1 / MovementSpeed);
	AddSnakeHead();
	AddSnakeParts(5);
}

// Called every frame
void ASnakeActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeActorBase::AddSnakeParts(int count)
{
	for (int i = 0; i < count; ++i)
	{
		FVector LocationOffset(SnakePartSize * SnakeParts.Num(), 0, 0);
		FTransform NewTransform(LocationOffset);
		ASnakePartBase* NewPart = GetWorld()->SpawnActor<ASnakePartBase>(ASnakePartBaseUClass, NewTransform);
		SnakeParts.Add(NewPart);
	}
}

void ASnakeActorBase::AddSnakeHead()
{
	if (SnakeParts.Num() == 0)
	{
		AddSnakeParts();
		SnakeParts[0]->SetTypeOfHead();
	}
}

void ASnakeActorBase::Move()
{
	for (int i = SnakeParts.Num() - 1; i > 0; --i)
		SnakeParts[i]->SetActorLocation(SnakeParts[i-1]->GetActorLocation());
	FVector SnakeOffsetVector(0, 0, 0);
	switch (LastDirection)
	{
	case EMovementDirection::UP:
		SnakeOffsetVector.X += SnakePartSize;
		break;
	case EMovementDirection::DOWN:
		SnakeOffsetVector.X -= SnakePartSize;
		break;
	case EMovementDirection::LEFT:
		SnakeOffsetVector.Y += SnakePartSize;
		break;
	case EMovementDirection::RIGHT:
		SnakeOffsetVector.Y -= SnakePartSize;
		break;
	}
	SnakeParts[0]->AddActorWorldOffset(SnakeOffsetVector);
}

