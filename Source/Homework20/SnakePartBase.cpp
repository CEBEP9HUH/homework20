// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePartBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakePartBase::ASnakePartBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ASnakePartBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakePartBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakePartBase::SetTypeOfHead_Implementation()
{
}

