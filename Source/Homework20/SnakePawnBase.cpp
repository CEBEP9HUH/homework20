// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePawnBase.h"
#include "SnakeActorBase.h"

#include "Engine/Classes/Camera/CameraComponent.h"

// Sets default values
ASnakePawnBase::ASnakePawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("player_camera"));
	RootComponent = CameraComponent;
}

// Called when the game starts or when spawned
void ASnakePawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	SnakeActor = GetWorld()->SpawnActor<ASnakeActorBase>(ASnakeActorBaseUClass, FTransform());
}

// Called every frame
void ASnakePawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASnakePawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &ASnakePawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &ASnakePawnBase::HandlePlayerHorizontalInput);
}

void ASnakePawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastDirection != EMovementDirection::DOWN)
			SnakeActor->LastDirection = EMovementDirection::UP;
		if (value < 0 && SnakeActor->LastDirection != EMovementDirection::UP)
			SnakeActor->LastDirection = EMovementDirection::DOWN;
	}
}

void ASnakePawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastDirection != EMovementDirection::LEFT)
			SnakeActor->LastDirection = EMovementDirection::RIGHT;
		if (value < 0 && SnakeActor->LastDirection != EMovementDirection::RIGHT)
			SnakeActor->LastDirection = EMovementDirection::LEFT;
	}
}