// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActorBase.generated.h"

class ASnakePartBase;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class HOMEWORK20_API ASnakeActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeActorBase();

	UPROPERTY()
		TArray<ASnakePartBase*> SnakeParts;

	UPROPERTY()
		EMovementDirection LastDirection;

	UPROPERTY(EditDefaultsOnly)
		float SnakePartSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakePartBase> ASnakePartBaseUClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeParts(int count = 1);
	void AddSnakeHead();
	void Move();
};
